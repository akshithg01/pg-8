#include<stdio.h>
void compute(int *a,int *b,int *c,int *d,int *e);
int main()
{
    int a,b,c,d,e;
    printf("Enter the two numbers to perform arithmetic operations\n");
    scanf("%d%d",&a,&b);
    compute(&a,&b,&c,&d,&e);
    printf("\nsum = %d\ndifference = %d\nProduct = %d\n",c,d,e);
    return 0;
}
void compute(int *a,int *b,int *c,int *d,int *e)
{
    *c=*a + *b;
    *d=*a - *b;
    *e=*a * *b;
    
}
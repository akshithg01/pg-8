#include<stdio.h>
void compute(int *a, int *b);
int main()
{
    int a,b;
    printf("Enter the value of the 2 numbers \n");
    scanf("%d%d",&a,&b);
    printf("The two numbers before swapping are\nNumber 1 = %d\nNumber 2 = %d\n",a,b);
    compute(&a,&b);
    printf("The two numbers after swapping are \nNumber 1 = %d\nNumber 2 = %d\n",a,b);
    return 0;
}
void compute(int *a,int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}
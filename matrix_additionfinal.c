#include<stdio.h>
int main()
{
    int r1,r2,c1,c2,i,j;
    printf("Enter the rows and columns of the first matrix\n");
    scanf("%d%d",&r1,&c1);
    printf("Enter the rows and columns of the second matrix\n");
    scanf("%d%d",&r2,&c2);
    int a[r1][c1],b[r2][c2],c[10][10];
    if (r1!=r2 ||c1!=c2)
    {
        printf("The operation is not possible\n");
        exit(0);
    }
    printf("ENter first matrix elements\n");
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("Enter the second matrix elements\n");
    for(i=0;i<r2;i++)
    {
        for(j=0;j<c2;j++)
        {
            scanf("%d",&b[i][j]);
            
        }
    }
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c2;j++)
        {
            c[i][j]=a[i][j]+b[i][j];
        }
    }
    printf("The resultant matrix is\n");
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c2;j++)
        {
            printf("%d  ",c[i][j]);
        }
        printf("\n");
    }
    return 0;
}    